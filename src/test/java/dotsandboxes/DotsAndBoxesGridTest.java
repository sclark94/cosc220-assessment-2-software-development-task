package dotsandboxes;

import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assumptions.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class DotsAndBoxesGridTest {
    /*
     * Because Test classes are classes, they can have fields, and can have static fields.
     * This field is a logger. Loggers are like a more advanced println, for writing messages out to the console or a log file.
     */
    private static final Logger logger = LogManager.getLogger(DotsAndBoxesGridTest.class);

    /*
     * Tests are functions that have an @Test annotation before them.
     * The typical format of a test is that it contains some code that does something, and then one
     * or more assertions to check that a condition holds.
     *
     * This is a dummy test just to show that the test suite itself runs
     */
    @Test
    public void testTestSuiteRuns() {
        logger.info("Dummy test to show the test suite runs");
        assertTrue(true);
    }

    // FIXME: You need to write tests for the two known bugs in the code.
    @Test
    public void testBoxCompleteAlgorithm() {
        DotsAndBoxesGrid grid = new DotsAndBoxesGrid(15, 8, 2);

        assertFalse(grid.boxComplete(0, 0));
        grid.drawVertical(0, 0, 1);
        assertFalse(grid.boxComplete(0, 0));

        assertFalse(grid.boxComplete(1, 0));
        grid.drawVertical(1, 0, 1);
        assertFalse(grid.boxComplete(1, 0));

        assertFalse(grid.boxComplete(0, 0));
        grid.drawHorizontal(0, 0, 1);
        assertFalse(grid.boxComplete(0, 0));

        assertFalse(grid.boxComplete(0, 1));
        grid.drawHorizontal(0, 1, 1);
        assertTrue(grid.boxComplete(0, 0));

        assertFalse(grid.boxComplete(5, 5));
        grid.drawHorizontal(5, 5, 1);
        assertFalse(grid.boxComplete(5, 5));

        assertFalse(grid.boxComplete(5, 6));
        grid.drawHorizontal(5, 6, 1);
        assertFalse(grid.boxComplete(5, 6));

        assertFalse(grid.boxComplete(5, 5));
        grid.drawVertical(5, 5, 1);
        assertFalse(grid.boxComplete(5, 5));

        assertFalse(grid.boxComplete(4, 5));
        grid.drawVertical(6, 5, 1);
        assertFalse(grid.boxComplete(6, 5));

        assertTrue(grid.boxComplete(5, 5));
    }

    @Test
    public void testDrawAlreadyDrawnLine() {
        DotsAndBoxesGrid grid = new DotsAndBoxesGrid(2, 2, 2);

        // Draw a line at (0, 0) and claim a box
        grid.drawHorizontal(0, 0, 1);

        // Try to draw the same line again, it should throw an IllegalStateException
        assertThrows(IllegalStateException.class, () -> grid.drawHorizontal(0, 0, 1));

        // Draw a vertical line at (0, 0) and claim a box
        grid.drawVertical(0, 0, 1);

        // Try to draw the same vertical line again, it should throw an IllegalStateException
        assertThrows(IllegalStateException.class, () -> grid.drawVertical(0, 0, 1));
    }

}
